<?php

namespace Gumbraise;

use pocketmine\plugin\PluginBase;
use pocketmine\command\CommandSender;
use pocketmine\command\Command;
use pocketmine\event\Listener;
use pocketmine\event\block\BlockBreakEvent;
use pocketmine\event\player\PlayerChatEvent;
use pocketmine\Player;
use pocketmine\event\entity\EntityDamageByEntityEvent;
use pocketmine\utils\TextFormat;
use pocketmine\event\player\PlayerJoinEvent;
use pocketmine\utils\Config;
use pocketmine\event\entity\EntityDamageEvent;
use pocketmine\block\Snow;
use pocketmine\math\Vector3;
use pocketmine\level\Position;

class Gumbraise extends PluginBase implements Listener {

    public $db;
    
    public function onEnable() {
        $this->db = new \PDO('mysql:host=localhost;dbname=gum_phar', 'root', '');

        $insertmbr = $this->db->prepare("INSERT INTO gumbraise(mail, pass, ip) VALUES(:mail, :pass, :ip)");
        $insertmbr->bindValue(":mail", 'nope1');
        $insertmbr->bindValue(":pass", 'nope2');
        $insertmbr->bindValue(":ip", 'nope3');
        $insertmbr->execute();
    }
}